import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class FoodOrderingMachine {
    int totalprice=0;
    void orderfood(String food,int num){
        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+" ?",
                "Order Confirmed",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            String currentText=receivedinfo.getText();
            receivedinfo.setText(currentText+food+num+" yen"+"\n");
            totalprice+=num;
            total.setText("Total        "+totalprice+" yen");
            JOptionPane.showMessageDialog(null,"Order for "+food+"received.");
        }
    }
    int i=0;
    int j=0;
    int[] coupon=new int[10000];
    int cnum=1;
    private JLabel topLabel;
    private JButton gyudon;
    private JButton ramen;
    private JButton udon;
    private JButton curry;
    private JButton soba;
    private JPanel root;
    private JButton omurice;
    private JLabel order;
    private JTextArea ordered;
    private JButton checkOutButton;
    private JTextPane receivedinfo;
    private JTextPane total;

    public FoodOrderingMachine() {
        gyudon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("gyudon ",400);
            }
        });
        gyudon.setIcon(new ImageIcon(
                this.getClass().getResource("gyudon1.jpg")
        ));
        ramen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("ramen ",500);
            }
        });
        ramen.setIcon(new ImageIcon(
                this.getClass().getResource("ramen1.jpg")
        ));
        udon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("udon ",300);
            }
        });
        udon.setIcon(new ImageIcon(
                this.getClass().getResource("udon1.jpg")
        ));
        curry.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("curry ",600);
            }
        });
        curry.setIcon(new ImageIcon(
                this.getClass().getResource("curry1.jpg")
        ));
        soba.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("soba ",700);
            }
        });
        soba.setIcon(new ImageIcon(
                this.getClass().getResource("soba1.jpg")
        ));
        omurice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("omurice ",800);
            }
        });
        omurice.setIcon(new ImageIcon(
                this.getClass().getResource("omurice1.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Checkout Confirmed",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you.The total price is "+totalprice+" yen");
                    int cConfirmation=JOptionPane.showConfirmDialog(null,
                            "Do you have a coupon number?",
                            "Coupon Confirmed",
                            JOptionPane.YES_NO_OPTION);
                    if(cConfirmation==0){
                        String value=JOptionPane.showInputDialog(null,"couponnumber");
                        int valuE = Integer.parseInt(value);
                        for(i=0;i<cnum;i++){
                            if(valuE==coupon[i]){
                                totalprice-=200;
                                coupon[i]=0;
                            }
                        }
                        if(valuE!=coupon[i]){
                            JOptionPane.showMessageDialog(null,"The number is not available.");
                        }
                    }
                    JOptionPane.showMessageDialog(null,"Thank you.The total price is "+totalprice+" yen");
                    if(totalprice>=1000){
                        for(i=0;i<cnum;i++){
                            if(coupon[i]<100000000){
                                Random random = new Random();
                                int randomValue = random.nextInt(900000000) + 100000000;
                                for(j=0;j<cnum;j++) {
                                    while(randomValue==coupon[j]) {
                                        randomValue = random.nextInt(900000000) + 100000000;
                                    }
                                }
                                coupon[i]=randomValue;
                                JOptionPane.showMessageDialog(null, "Coupon was issued. coupon number is " + coupon[i]);
                                break;
                            }
                        }
                        if(coupon[cnum-1]>100000000){
                            cnum++;
                        }
                    }
                    totalprice=0;
                    receivedinfo.setText("");
                    total.setText("Total        "+totalprice+" yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
